class OrderSerializer < ActiveModel::Serializer
  attributes :id, :total
  has_many :teaches, serializer: OrderTeachSerializer
end
