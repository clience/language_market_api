class User < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader
  validates :auth_token, uniqueness: true
  validates :mobile, uniqueness: true
  validates :gender, presence: true

  mount_uploader :avatar, AvatarUploader
  validates :avatar,
            :presence => false ,
            :file_size => { :maximum => 5.megabytes.to_i}
  validates_integrity_of :avatar
  validates_processing_of :avatar

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :generate_authentication_token!
  has_many :teaches, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :learns, dependent: :destroy

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

end