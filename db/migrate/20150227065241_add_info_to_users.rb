class AddInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :avatar, :string
    add_column :users, :gender, :string
    add_column :users, :mobile, :integer
    add_column :users, :intro, :string
  end
end
