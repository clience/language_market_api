class AddHourToPlacements < ActiveRecord::Migration
  def change
    add_column :placements, :hour, :integer, default: 0
  end
end
