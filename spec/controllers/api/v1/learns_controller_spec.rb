require 'spec_helper'

describe Api::V1::LearnsController do

  describe "GET #show" do
    before(:each) do
      @learn = FactoryGirl.create :learn
      get :show, id: @learn.id
    end

    it "returns the information about a reporter on a hash" do
      learn_response = json_response[:learn]
      expect(learn_response[:title]).to eql @learn.title
    end

    it "has the user as a embeded object" do
      learn_response = json_response[:learn]
      expect(learn_response[:user][:email]).to eql @learn.user.email
    end

    it { should respond_with 200 }
  end

  describe "GET #index" do
    before(:each) do
      4.times { FactoryGirl.create :learn }
    end

    context "when is not receiving any learn_ids parameter" do
      before(:each) do
        get :index
      end

      it "returns 4 records from the database" do
        learns_response = json_response
        expect(learns_response[:learns]).to have(4).items
      end

      it "returns the user object into each learn" do
        learns_response = json_response[:learns]
        learns_response.each do |learn_response|
          expect(learn_response[:user]).to be_present
        end
      end

      it { should respond_with 200 }
    end

    context "when learn_ids parameter is sent" do
      before(:each) do
        @user = FactoryGirl.create :user
        3.times { FactoryGirl.create :learn, user: @user }
        get :index, learn_ids: @user.learn_ids
      end

      it "returns just the learns that belong to the user" do
        learns_response = json_response[:learns]
        learns_response.each do |learn_response|
          expect(learn_response[:user][:email]).to eql @user.email
        end
      end
    end


  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        user = FactoryGirl.create :user
        @learn_attributes = FactoryGirl.attributes_for :learn
        api_authorization_header user.auth_token
        post :create, { user_id: user.id, learn: @learn_attributes }
      end

      it "renders the json representation for the learn record just created" do
        learn_response = json_response[:learn]
        expect(learn_response[:title]).to eql @learn_attributes[:title]
      end

      it { should respond_with 201 }
    end
  end

  describe "PUT/PATCH #update" do
    before(:each) do
      @user = FactoryGirl.create :user
      @learn = FactoryGirl.create :learn, user: @user
      api_authorization_header @user.auth_token
    end

    context "when is successfully updated" do
      before(:each) do
        patch :update, { user_id: @user.id, id: @learn.id, learn: { title: "An expensive TV" } }
      end

      it "renders the json representation for the updated user" do
        learn_response = json_response[:learn]
        expect(learn_response[:title]).to eql "An expensive TV"
      end

      it { should respond_with 200 }
    end

  end

  describe "DELETE #destroy" do
    before(:each) do
      @user = FactoryGirl.create :user
      @learn = FactoryGirl.create :learn, user: @user
      api_authorization_header @user.auth_token
      delete :destroy, { user_id: @user.id, id: @learn.id }
    end

    it { should respond_with 204 }
  end

end