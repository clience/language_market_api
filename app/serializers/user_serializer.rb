class UserSerializer < ActiveModel::Serializer
  embed :ids
  attributes :id, :email, :created_at, :updated_at, :auth_token, :avatar, :mobile, :gender

  has_many :teaches
  has_many :learns
end
