# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :learn do
    title { Faker::Product.product_name}
    user
  end
end
