require 'api_constraints'

LanguageMarketApi::Application.routes.draw do
  mount SabisuRails::Engine => "/sabisu_rails"
  devise_for :users
  # Api definition
  namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/'  do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :users, :only => [:show, :create, :update, :destroy] do
        resources :teaches, :only => [:create, :update, :destroy]
        resources :orders, :only => [:index, :show, :create]
        resources :learns, :only => [:create, :update, :destroy]
        resources :profile, :only => [:index]
      end
      resources :sessions, :only => [:create, :destroy]
      resources :teaches, :only => [:show, :index]
      resources :learns, :only => [:show, :index]
    end
  end
end