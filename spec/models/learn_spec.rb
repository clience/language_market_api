require 'spec_helper'

describe Learn do
  let(:learn) { FactoryGirl.build :learn }
  subject { learn }

  it { should respond_to(:title) }
  it { should respond_to(:user_id) }

  it { should validate_presence_of :title}
  it { should validate_presence_of :user_id}

  it { should belong_to :user }

  describe ".filter_by_title" do
    before(:each) do
      @learn1 = FactoryGirl.create :learn, title: "A plasma TV"
      @learn2 = FactoryGirl.create :learn, title: "Fastest Laptop"
      @learn3 = FactoryGirl.create :learn, title: "CD player"
      @learn4 = FactoryGirl.create :learn, title: "LCD TV"
    end

    context "when a 'TV' title pattern is sent" do
      it "returns the 2 learns matching" do
        expect(Learn.filter_by_title("TV")).to have(2).items
      end

      it "returns the learns matching" do
        expect(Learn.filter_by_title("TV").sort).to match_array([@learn1, @learn4])
      end
    end
  end

  describe ".recent" do
    before(:each) do
      @learn1 = FactoryGirl.create :learn
      @learn2 = FactoryGirl.create :learn
      @learn3 = FactoryGirl.create :learn
      @learn4 = FactoryGirl.create :learn

      @learn2.touch
      @learn3.touch
    end

    it "returns the most updated records" do
      expect(Learn.recent).to match_array([@learn3, @learn2, @learn4, @learn1])
    end
  end

  describe ".search" do
    before(:each) do
      @learn1 = FactoryGirl.create :learn, title: "Plasma tv"
      @learn2 = FactoryGirl.create :learn, title: "Videogame console"
      @learn3 = FactoryGirl.create :learn, title: "MP3"
      @learn4 = FactoryGirl.create :learn, title: "Laptop"
    end

    context "when an empty hash is sent" do
      it "returns all the learns" do
        expect(Learn.search({})).to match_array([@learn1, @learn2, @learn3, @learn4])
      end
    end

    context "when learn_ids is present" do
      it "returns the learn from the ids" do
        search_hash = { learn_ids: [@learn1.id, @learn2.id]}
        expect(Learn.search(search_hash)).to match_array([@learn1, @learn2])
      end
    end
  end

end
