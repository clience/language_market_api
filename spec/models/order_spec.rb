require 'spec_helper'

describe Order do
  let(:order) { FactoryGirl.build :order }
  subject { order }

  it { should respond_to(:total) }
  it { should respond_to(:user_id) }

  it { should validate_presence_of :user_id }

  it { should belong_to :user }

  it { should have_many(:placements) }
  it { should have_many(:teaches).through(:placements) }

  describe '#set_total!' do
    before(:each) do
      teach_1 = FactoryGirl.create :teach, price: 100
      teach_2 = FactoryGirl.create :teach, price: 85

      placement_1 = FactoryGirl.build :placement, teach: teach_1, hour: 3
      placement_2 = FactoryGirl.build :placement, teach: teach_2, hour: 15

      @order = FactoryGirl.build :order

      @order.placements << placement_1
      @order.placements << placement_2
    end

    it "returns the total amount to pay for the teaches" do
      expect{@order.set_total!}.to change{@order.total.to_f}.from(0).to(1575)
    end
  end

  describe "#build_placements_with_teach_ids_and_quantities" do
    before(:each) do
      teach_1 = FactoryGirl.create :teach, price: 100, hour: 5
      teach_2 = FactoryGirl.create :teach, price: 85, hour: 10

      @teach_ids_and_hours = [[teach_1.id, 2], [teach_2.id, 3]]
    end

    it "builds 2 placements for the order" do
      expect{order.build_placements_with_teach_ids_and_hours(@teach_ids_and_hours)}.to change{order.placements.size}.from(0).to(2)
    end
  end

end