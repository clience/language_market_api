class CreateLearns < ActiveRecord::Migration
  def change
    create_table :learns do |t|
      t.string :title, default: ""
      t.integer :user_id

      t.timestamps
    end
    add_index :learns, :user_id
  end
end
