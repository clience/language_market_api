class Api::V1::TeachesController < ApplicationController
  before_action :authenticate_with_token!, only:  [:create, :update]
  respond_to :json

  def index
    respond_with Teach.search(params)
  end

  def show
    respond_with Teach.find(params[:id])
  end

  def create
    teach = current_user.teaches.build(teach_params)
    if teach.save
      render json: teach, status: 201, location: [:api, teach]
    else
      render json: { errors: teach.errors }, status: 422
    end
  end

  def update
    teach = current_user.teaches.find(params[:id])
    if teach.update(teach_params)
      render json: teach, status: 200, location: [:api, teach]
    else
      render json: { errors: teach.errors }, status: 422
    end
  end

  def destroy
    teach = current_user.teaches.find(params[:id])
    teach.destroy
    head 204
  end

  private

  def teach_params
    params.require(:teach).permit(:title, :price, :published)
  end

end
