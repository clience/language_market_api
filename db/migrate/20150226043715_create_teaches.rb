class CreateTeaches < ActiveRecord::Migration
  def change
    create_table :teaches do |t|
      t.string :title, default: ""
      t.decimal :price, default: 0.0
      t.boolean :published, default: false
      t.integer :user_id

      t.timestamps
    end
    add_index :teaches, :user_id
  end
end
