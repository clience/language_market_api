require 'spec_helper'

describe Teach do
  let(:teach) { FactoryGirl.build :teach }
  subject { teach }

  it { should respond_to(:title) }
  it { should respond_to(:price) }
  it { should respond_to(:published) }
  it { should respond_to(:user_id) }

  it { should validate_presence_of :title }
  it { should validate_presence_of :price }
  it { should validate_numericality_of(:price).is_greater_than_or_equal_to(0) }
  it { should validate_presence_of :user_id }

  it { should belong_to :user }
  it { should have_many(:placements) }
  it { should have_many(:orders).through(:placements) }

  describe ".filter_by_title" do
    before(:each) do
      @teach1 = FactoryGirl.create :teach, title: "A plasma TV"
      @teach2 = FactoryGirl.create :teach, title: "Fastest Laptop"
      @teach3 = FactoryGirl.create :teach, title: "CD player"
      @teach4 = FactoryGirl.create :teach, title: "LCD TV"

    end

    context "when a 'TV' title pattern is sent" do
      it "returns the 2 teaches matching" do
        expect(Teach.filter_by_title("TV")).to have(2).items
      end

      it "returns the teaches matching" do
        expect(Teach.filter_by_title("TV").sort).to match_array([@teach1, @teach4])
      end
    end
  end

  describe ".above_or_equal_to_price" do
    before(:each) do
      @teach1 = FactoryGirl.create :teach, price: 100
      @teach2 = FactoryGirl.create :teach, price: 50
      @teach3 = FactoryGirl.create :teach, price: 150
      @teach4 = FactoryGirl.create :teach, price: 99
    end

    it "returns the teaches which are above or equal to the price" do
      expect(Teach.above_or_equal_to_price(100).sort).to match_array([@teach1, @teach3])
    end
  end

  describe ".below_or_equal_to_price" do
    before(:each) do
      @teach1 = FactoryGirl.create :teach, price: 100
      @teach2 = FactoryGirl.create :teach, price: 50
      @teach3 = FactoryGirl.create :teach, price: 150
      @teach4 = FactoryGirl.create :teach, price: 99
    end

    it "returns the teaches which are above or equal to the price" do
      expect(Teach.below_or_equal_to_price(99).sort).to match_array([@teach2, @teach4])
    end
  end

  describe ".recent" do
    before(:each) do
      @teach1 = FactoryGirl.create :teach, price: 100
      @teach2 = FactoryGirl.create :teach, price: 50
      @teach3 = FactoryGirl.create :teach, price: 150
      @teach4 = FactoryGirl.create :teach, price: 99

      @teach2.touch
      @teach3.touch
    end

    it "returns the most updated records" do
      expect(Teach.recent).to match_array([@teach3, @teach2, @teach4, @teach1])
    end
  end

  describe ".search" do
    before(:each) do
      @teach1 = FactoryGirl.create :teach, price: 100, title: "Plasma tv"
      @teach2 = FactoryGirl.create :teach, price: 50, title: "Videogame console"
      @teach3 = FactoryGirl.create :teach, price: 150, title: "MP3"
      @teach4 = FactoryGirl.create :teach, price: 99, title: "Laptop"
    end

    context "when title 'videogame' and '100' a min price are set" do
      it "returns an empty array" do
        search_hash = { keyword: "videogame", min_price: 100 }
        expect(Teach.search(search_hash)).to be_empty
      end
    end

    context "when title 'tv', '150' as max price, and '50' as min price are set" do
      it "returns the teach1" do
        search_hash = { keyword: "tv", min_price: 50, max_price: 150 }
        expect(Teach.search(search_hash)).to match_array([@teach1])
      end
    end

    context "when an empty hash is sent" do
      it "returns all the teachs" do
        expect(Teach.search({})).to match_array([@teach1, @teach2, @teach3, @teach4])
      end
    end

    context "when teach_ids is present" do
      it "returns the teach from the ids" do
        search_hash = { teach_ids: [@teach1.id, @teach2.id]}
        expect(Teach.search(search_hash)).to match_array([@teach1, @teach2])
      end
    end
  end


end
