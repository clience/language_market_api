class Api::V1::LearnsController < ApplicationController
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]
  respond_to :json

  def index
    respond_with Learn.search(params)
  end

  def show
    respond_with Learn.find(params[:id])
  end

  def create
    learn = current_user.learns.build(learn_params)
    if learn.save
      render json: learn, status: 201, location: [:api, learn]
    else
      render json: { errors: learn.errors }, status: 422
    end
  end

  def update
    learn = current_user.learns.find(params[:id])
    if learn.update(learn_params)
      render json: learn, status: 200, location: [:api, learn]
    else
      render json: { errors: learn.errors }, status: 422
    end
  end

  def destroy
    learn = current_user.learns.find(params[:id])
    learn.destroy
    head 204
  end

  private

  def learn_params
    params.require(:learn).permit(:title)
  end

end