FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password "12345678"
    password_confirmation "12345678"
    avatar nil
    gender "Male"
    mobile { Faker::PhoneNumberSG.mobile_number }
    intro { Faker::Lorem.paragraph }
  end
end