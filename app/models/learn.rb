class Learn < ActiveRecord::Base
  validates :title, :user_id, presence: true
  belongs_to :user

  scope :filter_by_title, lambda { |keyword| where("lower(title) LIKE ?", "%#{keyword.downcase}%" ) }
  scope :recent, -> { order(:updated_at) }

  def self.search(params = {})
    learns = params[:learn_ids].present? ? Learn.find(params[:learn_ids]) : Learn.all

    learns = learns.filter_by_title(params[:keyword]) if params[:keyword]
    learns = learns.recent(params[:recent]) if params[:recent].present?

    learns
  end

end
