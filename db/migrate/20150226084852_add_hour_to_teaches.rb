class AddHourToTeaches < ActiveRecord::Migration
  def change
    add_column :teaches, :hour, :integer, default: 0
  end
end
