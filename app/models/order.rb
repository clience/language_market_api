class Order < ActiveRecord::Base
  belongs_to :user
  has_many :placements
  has_many :teaches, through: :placements

  validates :total, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :user_id, presence: true

  before_validation :set_total!

  def set_total!
    self.total = 0
    placements.each do |placement|
      self.total += placement.teach.price * placement.hour
    end
  end

  def build_placements_with_teach_ids_and_hours(teach_ids_and_hours)
    teach_ids_and_hours.each do |teach_id_and_hour|
      id, hour = teach_id_and_hour # [1,5]

      self.placements.build(teach_id: id, hour: hour)
    end
  end

end