require 'spec_helper'

describe Api::V1::TeachesController do

  describe "GET #show" do
    before(:each) do
      @teach = FactoryGirl.create :teach
      get :show, id: @teach.id
    end

    it "returns the information about a reporter on a hash" do
      teach_response = json_response[:teach]
      expect(teach_response[:title]).to eql @teach.title
    end

    it "has the user as a embeded object" do
      teach_response = json_response[:teach]
      expect(teach_response[:user][:email]).to eql @teach.user.email
    end

    it { should respond_with 200 }
  end

  describe "GET #index" do
    before(:each) do
      4.times { FactoryGirl.create :teach }
      get :index
    end

    it "returns 4 records from the database" do
      teaches_response = json_response
      expect(teaches_response[:teaches]).to have(4).items
    end

    it "returns the user object into each product" do
      teaches_response = json_response[:teaches]
      teaches_response.each do |teach_response|
        expect(teach_response[:user]).to be_present
      end
    end

    it { should respond_with 200 }
  end

  context "when teach_ids parameter is sent" do
    before(:each) do
      @user = FactoryGirl.create :user
      3.times { FactoryGirl.create :teach, user: @user }
      get :index, teach_ids: @user.teach_ids
    end

    it "returns just the teaches that belong to the user" do
      teaches_response = json_response[:teaches]
      teaches_response.each do |teach_response|
        expect(teach_response[:user][:email]).to eql @user.email
      end
    end
  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        user = FactoryGirl.create :user
        @teach_attributes = FactoryGirl.attributes_for :teach
        api_authorization_header user.auth_token
        post :create, { user_id: user.id, teach: @teach_attributes }
      end

      it "renders the json representation for the teach record just created" do
        teach_response = json_response[:teach]
        expect(teach_response[:title]).to eql @teach_attributes[:title]
      end

      it { should respond_with 201 }
    end

    context "when is not created" do
      before(:each) do
        user = FactoryGirl.create :user
        @invalid_teach_attributes = { title: "Smart TV", price: "Twelve dollars" }
        api_authorization_header user.auth_token
        post :create, { user_id: user.id, teach: @invalid_teach_attributes }
      end

      it "renders an errors json" do
        teach_response = json_response
        expect(teach_response).to have_key(:errors)
      end

      it "renders the json errors on whye the user could not be created" do
        teach_response = json_response
        expect(teach_response[:errors][:price]).to include "is not a number"
      end

      it { should respond_with 422 }
    end
  end

  describe "PUT/PATCH #update" do
    before(:each) do
      @user = FactoryGirl.create :user
      @teach = FactoryGirl.create :teach, user: @user
      api_authorization_header @user.auth_token
    end

    context "when is successfully updated" do
      before(:each) do
        patch :update, { user_id: @user.id, id: @teach.id,
                         teach: { title: "An expensive TV" } }
      end

      it "renders the json representation for the updated user" do
        teach_response = json_response[:teach]
        expect(teach_response[:title]).to eql "An expensive TV"
      end

      it { should respond_with 200 }
    end

    context "when is not updated" do
      before(:each) do
        patch :update, { user_id: @user.id, id: @teach.id,
                         teach: { price: "two hundred" } }
      end

      it "renders an errors json" do
        teach_response = json_response
        expect(teach_response).to have_key(:errors)
      end

      it "renders the json errors on whye the user could not be created" do
        teach_response = json_response
        expect(teach_response[:errors][:price]).to include "is not a number"
      end

      it { should respond_with 422 }
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @user = FactoryGirl.create :user
      @teach = FactoryGirl.create :teach, user: @user
      api_authorization_header @user.auth_token
      delete :destroy, { user_id: @user.id, id: @teach.id }
    end

    it { should respond_with 204 }
  end

end