class Teach < ActiveRecord::Base
  validates :title, :user_id, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0 },
            presence: true
  belongs_to :user
  has_many :placements
  has_many :orders, through: :placements

  scope :filter_by_title, lambda { |keyword| where("lower(title) LIKE ?", "%#{keyword.downcase}%" ) }
  scope :above_or_equal_to_price, lambda { |price| where("price >= ?", price) }
  scope :below_or_equal_to_price, lambda { |price| where("price <= ?", price) }
  scope :recent, -> { order(:updated_at) }

  def self.search(params = {})
    teaches = params[:teach_ids].present? ? Teach.find(params[:teach_ids]) : Teach.all

    teaches = teaches.filter_by_title(params[:keyword]) if params[:keyword]
    teaches = teaches.above_or_equal_to_price(params[:min_price].to_f) if params[:min_price]
    teaches = teaches.below_or_equal_to_price(params[:max_price].to_f) if params[:max_price]
    teaches = teaches.recent(params[:recent]) if params[:recent].present?

    teaches
  end

end
